﻿namespace ShellInstaller
{
    internal sealed class InstallerSettings
    {
        public PathVariableInfo? PathVariable { get; set; }

        public FileContextMenuInfo? FileContextMenu { get; set; }


        internal sealed class PathVariableInfo
        {
            public bool AddPathVariable { get; set; }

            public string? ApplicationDirectory { get; set; }
        }

        internal sealed class FileContextMenuInfo
        {
            public bool AddFileContextMenu { get; set; }

            public string? ApplicationName { get; set; }

            public string? ApplicationPath { get; set; }

            public string? ContextMenuDescription { get; set; }

            public bool ShowOnlyAtShift { get; set; }
        }
    }
}
