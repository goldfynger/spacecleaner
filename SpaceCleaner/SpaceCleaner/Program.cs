﻿using System;
using System.IO;
using System.Linq;
using System.Text.Json;
using System.Text.RegularExpressions;

using Goldfynger.Utils.Extensions;

namespace SpaceCleaner
{
    public class Program
    {
        public static void Main(string[] args)
        {
            string targetFilePath;
            try
            {
                targetFilePath = GetTargetFilePath(args);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                WaitForUserClose();
                return;
            }


            var executablePath = Environment.GetCommandLineArgs()[0];
            var executableName = Path.GetFileNameWithoutExtension(executablePath);

            var appDataDirectoryPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), executableName);

            var settingsFilePath = Path.Combine(appDataDirectoryPath, "settings.json");

            Settings settings;
            try
            {
                settings = GetSettings(settingsFilePath);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                WaitForUserClose();
                return;
            }


            if (settings.KnownExtensions == null || settings.KnownExtensions.Length == 0)
            {
                Console.WriteLine("Deserialized settings have no known extensions.");
                WaitForUserClose();
                return;
            }

            if (!settings.KnownExtensions.All(s => CheckExtension(s)))
            {
                Console.WriteLine("Deserialized settings have at least one invalid extension.");
                WaitForUserClose();
                return;
            }

            var targetExtension = Path.GetExtension(targetFilePath);

            if (!settings.KnownExtensions.Any(s => s.Equals(targetExtension)))
            {
                Console.WriteLine($"Target file {targetFilePath} has extension that not exist in list of known extensions.");
                WaitForUserClose();
                return;
            }


            string backupFilePath;
            try
            {
                backupFilePath = MakeBackupFilePath(targetFilePath, appDataDirectoryPath);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                WaitForUserClose();
                return;
            }

            try
            {
                BackupTargetFile(targetFilePath, backupFilePath);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                WaitForUserClose();
                return;
            }


            var originalFileContent = File.ReadAllText(backupFilePath);

            var trimmed = originalFileContent.TrimEndMultiline();

            if (originalFileContent.Length == trimmed.Length)
            {
                RemoveBackupFile(backupFilePath);
            }
            else
            {
                try
                {
                    File.WriteAllText(targetFilePath, trimmed);
                }
                catch (Exception ex)
                {
                    Console.WriteLine($"Can not write trimmed content to file {targetFilePath}: {ex.Message}.");
                    WaitForUserClose();
                    return;
                }
            }
        }


        private static void WaitForUserClose()
        {
            Console.WriteLine("Press ENTER to close application...");
            Console.ReadLine();
        }

        private static bool CheckExtension(string? extension)
        {
            if (extension == null)
            {
                return false;
            }

            if (extension.Length == 0)
            {
                return true;
            }

            if (extension[0] != '.' || extension.Length < 2)
            {
                return false;
            }

            return !Path.GetInvalidPathChars().Intersect(extension).Any();
        }

        private static string GetTargetFilePath(string[] args)
        {
            if (args.Length == 0 || args[0].Length == 0)
            {
                throw new ArgumentException("Input arguments are invalid: first argument must be path of target file.");
            }

            var targetFilePath = args[0];

            if (!File.Exists(targetFilePath))
            {
                throw new ArgumentException($"Input arguments are invalid: target file {targetFilePath} does not exist.");
            }

            return targetFilePath;
        }

        private static Settings GetSettings(string settingsFilePath)
        {
            Settings? settings;

            if (!File.Exists(settingsFilePath))
            {
                Console.WriteLine($"File with settings does not exist: creating new file {settingsFilePath}.");

                settings = new Settings { KnownExtensions = new string[] { ".c", ".h" } };

                string? jsonString;
                try
                {
                    jsonString = JsonSerializer.Serialize(settings);
                }
                catch (Exception ex)
                {
                    throw new InvalidOperationException($"An error occurred while serializing settings: {ex.Message}.", ex);
                }

                var settingsDirectoryPath = Path.GetDirectoryName(settingsFilePath);

                if (settingsDirectoryPath == null)
                {
                    throw new InvalidOperationException("Settings directory has null value.");
                }

                try
                {
                    Directory.CreateDirectory(settingsDirectoryPath);
                }
                catch (Exception ex)
                {
                    throw new InvalidOperationException($"An error occurred while creating directory for settings: {ex.Message}.", ex);
                }

                try
                {
                    File.WriteAllText(settingsFilePath, jsonString);
                }
                catch (Exception ex)
                {
                    throw new InvalidOperationException($"An error occurred while writing file with settings: {ex.Message}.", ex);
                }
            }
            else
            {
                string? jsonString;
                try
                {
                    jsonString = File.ReadAllText(settingsFilePath);
                }
                catch (Exception ex)
                {
                    throw new InvalidOperationException($"An error occurred while reading file with settings: {ex.Message}.", ex);
                }

                try
                {
                    settings = JsonSerializer.Deserialize<Settings>(jsonString);
                }
                catch (Exception ex)
                {
                    throw new InvalidOperationException($"An error occurred while deserializing settings: {ex.Message}.", ex);
                }

                if (settings == null)
                {
                    throw new InvalidOperationException("Deserialized settings have null value.");
                }
            }

            return settings;
        }

        private static string MakeBackupFilePath(string targetFilePath, string appDataDirectoryPath)
        {
            var rootRegex = new Regex(@"^[A-Z]{1}:\\$", RegexOptions.IgnoreCase);

            var targetRoot = Path.GetPathRoot(targetFilePath);

            if (string.IsNullOrWhiteSpace(targetRoot) || !rootRegex.IsMatch(targetRoot))
            {
                throw new ArgumentException("Target file root has invalid value.");
            }

            var targetDirectoryPath = Path.GetDirectoryName(targetFilePath);

            if (string.IsNullOrWhiteSpace(targetDirectoryPath))
            {
                throw new ArgumentException("Target file directory has invalid value.");
            }

            var targetName = Path.GetFileName(targetFilePath);

            if (string.IsNullOrWhiteSpace(targetName))
            {
                throw new ArgumentException("Target file name has invalid value.");
            }

            return Path.Combine(appDataDirectoryPath, $"Drive{char.ToUpper(targetRoot[0])}", targetDirectoryPath[3..], $"{targetName}.{DateTime.Now:yyyy.MM.dd.HH.mm.ss.fff}.backup");
        }

        private static void BackupTargetFile(string targetFilePath, string backupFilePath)
        {
            var backupDirectoryPath = Path.GetDirectoryName(backupFilePath)!;

            try
            {
                Directory.CreateDirectory(backupDirectoryPath);
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException($"An error occurred while creating directory for backup: {ex.Message}.", ex);
            }

            try
            {
                File.Copy(targetFilePath, backupFilePath);
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException($"An error occurred while copying backup file: {ex.Message}.", ex);
            }
        }

        private static void RemoveBackupFile(string backupFilePath)
        {
            /* Here we deleting only backup file without deleting empty folders: if we try to run several instances of application,
             * one instance can remove directory after anoter isntance checks that directory is exists; another isntance will crash. */

            try
            {
                File.Delete(backupFilePath);
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException($"An error occurred while deleting backup file: {ex.Message}.", ex);
            }
        }
    }
}
