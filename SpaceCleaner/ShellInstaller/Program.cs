﻿using System;
using System.IO;
using System.Text.Json;

using Microsoft.Win32;

namespace ShellInstaller
{
    public class Program
    {
        private const string InstallerSettingsFileName = "InstallerSettings.json";


        public static void Main()
        {
            string? jsonString;
            try
            {
                jsonString = File.ReadAllText(InstallerSettingsFileName);
            }
            catch (Exception ex)
            {
                Console.WriteLine($"An error occured while reading file with installer settings: {ex.Message}.");
                return;
            }

            InstallerSettings? installerSettings;
            try
            {
                installerSettings = JsonSerializer.Deserialize<InstallerSettings>(jsonString);
            }
            catch (Exception ex)
            {
                Console.WriteLine($"An error occured while deserializing installer settings: {ex.Message}.");
                return;
            }

            if (installerSettings != null)
            {
                TryAddPathVariable(installerSettings.PathVariable);
                TryAddFileContextMenu(installerSettings.FileContextMenu);
            }

            Console.WriteLine("Press ENTER to close application...");
            Console.ReadLine();
        }


        /// <summary>Tries to add specified directory path to current user PATH environment variable.</summary>
        /// <param name="info">Instance of <see cref="InstallerSettings.PathVariableInfo"/>.</param>
        private static void TryAddPathVariable(InstallerSettings.PathVariableInfo? info)
        {
            if (info != null)
            {
                if (info.AddPathVariable)
                {
                    if (!Directory.Exists(info.ApplicationDirectory))
                    {
                        Console.WriteLine($"Can not add directory to PATH: \"{info.ApplicationDirectory}\" does not exist.");
                        return;
                    }

                    const string EnvironmentVariableName = "PATH";
                    const EnvironmentVariableTarget EnvironmentVariableScope = EnvironmentVariableTarget.User;

                    var environmentVariableContent = Environment.GetEnvironmentVariable(EnvironmentVariableName, EnvironmentVariableScope);

                    if (environmentVariableContent != null)
                    {
                        var found = false;

                        foreach (var part in environmentVariableContent.Split(';'))
                        {
                            if (part.Equals(info.ApplicationDirectory, StringComparison.OrdinalIgnoreCase))
                            {
                                found = true;
                                break;
                            }
                        }

                        if (found)
                        {
                            Console.WriteLine($"Directory \"{info.ApplicationDirectory}\" already in PATH.");
                        }
                        else
                        {
                            if (environmentVariableContent[^1] != ';')
                            {
                                environmentVariableContent += ';';
                            }

                            environmentVariableContent += info.ApplicationDirectory;

                            try
                            {
                                Environment.SetEnvironmentVariable(EnvironmentVariableName, environmentVariableContent, EnvironmentVariableScope);
                                Console.WriteLine($"Directory \"{info.ApplicationDirectory}\" successfully added in PATH.");
                            }
                            catch (Exception ex)
                            {
                                Console.WriteLine($"An error occured while adding directory to PATH: {ex.Message}.");
                            }
                        }
                    }
                }
            }
        }

        /// <summary>Tries to add new item for file context menu.</summary>
        /// <param name="info">Instance of <see cref="InstallerSettings.FileContextMenuInfo"/>.</param>
        /// <remarks>https://stackoverflow.com/questions/20449316/how-add-context-menu-item-to-windows-explorer-for-folders</remarks>
        private static void TryAddFileContextMenu(InstallerSettings.FileContextMenuInfo? info)
        {
            if (info != null)
            {
                if (info.AddFileContextMenu)
                {
                    if (string.IsNullOrWhiteSpace(info.ApplicationName))
                    {
                        Console.WriteLine($"Can not add file context menu item: {nameof(InstallerSettings.FileContextMenuInfo.ApplicationName)} is not set or has invalid value.");
                        return;
                    }

                    if (!File.Exists(info.ApplicationPath))
                    {
                        Console.WriteLine($"Can not add file context menu item: {nameof(InstallerSettings.FileContextMenuInfo.ApplicationPath)} is not set or file does not exist.");
                        return;
                    }

                    if (string.IsNullOrWhiteSpace(info.ContextMenuDescription))
                    {
                        Console.WriteLine($"Can not add file context menu item: {nameof(InstallerSettings.FileContextMenuInfo.ContextMenuDescription)} is not set or has invalid value.");
                        return;
                    }

                    const string RegistryRoot = @"HKEY_CURRENT_USER\SOFTWARE\Classes\*\shell\";
                    const string ShowOnlyAtShiftValueName = "Extended";

                    var baseKey = RegistryRoot + info.ApplicationName;
                    var commandKey = baseKey + @"\command";

                    try
                    {
                        Registry.SetValue(baseKey, string.Empty, info.ContextMenuDescription);
                        if (info.ShowOnlyAtShift)
                        {
                            Registry.SetValue(baseKey, ShowOnlyAtShiftValueName, string.Empty);
                        }
                        Registry.SetValue(commandKey, string.Empty, $"\"{info.ApplicationPath}\" \"%1\"");
                        Console.WriteLine($"Keys \"{baseKey}\" and \"{commandKey}\" successfully set.");
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine($"An error occured while adding file context menu item: {ex.Message}.");
                    }
                }
            }
        }
    }
}
