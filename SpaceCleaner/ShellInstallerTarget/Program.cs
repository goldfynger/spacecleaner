﻿using System;

namespace ShellInstallerTarget
{
    public class Program
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("Arguments:");

            foreach (var arg in args)
            {
                Console.WriteLine(arg);
            }

            Console.WriteLine("Press ENTER to close application...");
            Console.ReadLine();
        }
    }
}
